all:
	go build -o ./bin/main -gcflags=all="-N -l" main.go
	gcc -o ./bin/gladewin -g ./gtk/main.c -Wall `pkg-config --cflags --libs gtk+-3.0` -export-dynamic
clean:
	rm -rf ./bin/main
	rm -rf cscope*
	rm -rf ./bin/gladewin
	rm -rf ui/ui
cscope:
	find . -name "*.go" > cscope.files
	cscope -b -q -k

