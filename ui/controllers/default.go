package controllers

import (
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "rummy.me"
	c.Data["Email"] = "gunjikar@gmail.com"
	c.TplName = "index.html"
}
