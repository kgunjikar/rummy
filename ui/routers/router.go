package routers

import (
	"bitbucket.org/rummy/ui/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
}
