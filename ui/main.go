package main

import (
	_ "bitbucket.org/rummy/ui/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}

