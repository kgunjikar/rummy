package main

import (
	"bitbucket.org/rummy/game"
	"fmt"
)

func main() {
	var i int

	fmt.Println("Choices: ")
	fmt.Println("         1) New Game ")
	fmt.Println("         2) Exit")
	fmt.Scanf("%d", &i)
	if i >= 2 {
		return
	}
	fmt.Println("Starting game")
	g := game.New_Game(2, 13)
	g.Play_Game()
}
