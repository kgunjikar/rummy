package deck

import (
	"bitbucket.org/rummy/cards"
	"fmt"
	"math/rand"
)

const (
	DECK_SIZE = 54
)

type Deck struct {
	cards []cards.Card
	top   int
}

func New_Deck(empty bool) *Deck {
	x := &Deck{}
	x.top = 0
	if !empty {
		for i := 1; i <= DECK_SIZE; i++ {
			c := cards.Card{}
			y := &c
			y.Set_Value(i)
			x.cards = append(x.cards, c)
		}
		x.Shuffle()
	}
	return x
}

func (deck *Deck) Set_Top(val int) {
	deck.top = val
}

func (deck *Deck) Cut(val int) {
	deck.cards = append(deck.cards[:val], deck.cards[val:]...)
	deck.top = 0
}

func (d *Deck) Print_Deck() {
	fmt.Println("Deck is len:%d", len(d.cards))
	for i := 0; i < len(d.cards); i++ {
		fmt.Printf("%d %s\n", i, d.cards[i].Get_Card())
	}
}

func (deck *Deck) Add_Top(card cards.Card) {
	x := []cards.Card{card}
	if card.Get_Value() == 0 {
		return
	}
	deck.cards = append(x, deck.cards...)
	deck.top = 0
}

func (deck *Deck) Remove_Top() cards.Card {
	var card cards.Card

	if len(deck.cards) == 0 {
		card.Set_Value(0)
		return card
	}
	card, deck.cards = deck.cards[0], deck.cards[1:]
	deck.top = 0
	return card
}

func (d *Deck) Is_Empty() bool {

	if len(d.cards) == 0 {
		return true
	}
	return false
}

func (deck *Deck) Show_Deck_Top() cards.Card {

	card := cards.Card{}
	card.Set_Value(0)
	if deck == nil || len(deck.cards) == 0 {
		return card
	}
	deck.cards[deck.top].Print_Card()
	return deck.cards[deck.top]
}

func (deck *Deck) Shuffle() {
	rand.Shuffle(DECK_SIZE, func(i, j int) {
		deck.cards[i], deck.cards[j] = deck.cards[j], deck.cards[i]
	})
}
