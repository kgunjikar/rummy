package utils

import (
	"bufio"
	"os"
	"strings"
)

func Read_Text() string {
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	text = strings.Replace(text, "\n", "", -1)
	return text
}

func Is_Yes(text string) bool {
	if strings.ContainsAny("Yes", text) ||
		strings.ContainsAny("Y", text) ||
		strings.ContainsAny("y", text) {
		return true
	} else {
		return false
	}
}
