#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h> 

GtkBuilder      *builder; 
int main(int argc, char *argv[]) {
    GtkWidget       *window;
 
    gtk_init(&argc, &argv);
 
    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "main_ui.glade", NULL);
 
    window = GTK_WIDGET(gtk_builder_get_object(builder, "Rummy"));
    gtk_builder_connect_signals(builder, NULL);
 
    g_object_unref(builder);
 
    gtk_widget_show(window);                
    gtk_main();
 
    return 0;
}
 // called when window is closed
void on_FileQuit_destroy() {
    fprintf(stderr, "FileQuit Destroy ");
     gtk_main_quit();
}

// called when window is closed
void on_Rummy_destroy() {
    fprintf(stderr, "Rummy Destroy ");
     gtk_main_quit();
}

void on_DeclareRummy_activate() {
    fprintf(stderr, "Clicked New");
}
void on_DeclareRummy_clicked() {
    fprintf(stderr, "Clicked New");
}

void on_FileNew_show() {
    fprintf(stderr, "Show New");
} 

void on_FileNew_activate(GtkApplicationWindow *w) {
    GtkWidget       *window;

    window = GTK_WIDGET(gtk_builder_get_object(builder, "NewGame"));
    gtk_builder_connect_signals(builder, NULL);
 
    g_object_unref(builder);
 
    gtk_widget_show(window);     
}

void on_FileQuit_activate() {
    fprintf(stderr, "Clicked New");
}
