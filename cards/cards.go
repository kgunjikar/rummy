package cards

import "fmt"

var card_strings map[int]string

const (
	SPADES   = iota
	CLUBS    = iota + 13
	DIAMONDS = iota + 26
	HEARTS   = iota + 39
	JOKER1   = iota + 52
	JOKER2   = iota + 53
)

type Card struct {
	value int
}

func (c *Card) Get_Value() int {
	return c.value
}
func (c *Card) Set_Value(i int) {
	c.value = i
}

func init() {
	card_strings = make(map[int]string, 55)
	card_strings[1] = "Ace of Spades"
	card_strings[2] = "Two of Spades"
	card_strings[3] = "Three of Spades"
	card_strings[4] = "Four of Spades"
	card_strings[5] = "Five of Spades"
	card_strings[6] = "Six of Spades"
	card_strings[7] = "Seven of Spades"
	card_strings[8] = "Eight of Spades"
	card_strings[9] = "Nine of Spades"
	card_strings[10] = "Ten of Spades"
	card_strings[11] = "Jack of Spades"
	card_strings[12] = "Queen of Spades"
	card_strings[13] = "King of Spades"

	card_strings[14] = "Ace of Clubs"
	card_strings[15] = "Two of Clubs"
	card_strings[16] = "Three of Clubs"
	card_strings[17] = "Four of Clubs"
	card_strings[18] = "Five of Clubs"
	card_strings[19] = "Six of Clubs"
	card_strings[20] = "Seven of Clubs"
	card_strings[21] = "Eight of Clubs"
	card_strings[22] = "Nine of Clubs"
	card_strings[23] = "Ten of Clubs"
	card_strings[24] = "Jack of Clubs"
	card_strings[25] = "Queen of Clubs"
	card_strings[26] = "King of Clubs"

	card_strings[27] = "Ace  of Diamonds"
	card_strings[28] = "Two of Diamonds"
	card_strings[29] = "Three of Diamonds"
	card_strings[30] = "Four of Diamonds"
	card_strings[31] = "Five of Diamonds"
	card_strings[32] = "Six of Diamonds"
	card_strings[33] = "Seven of Diamonds"
	card_strings[34] = "Eight of Diamonds"
	card_strings[35] = "Nine of Diamonds"
	card_strings[36] = "Ten of Diamonds"
	card_strings[37] = "Jack of Diamonds"
	card_strings[38] = "Queen of Diamonds"
	card_strings[39] = "King of Diamonds"

	card_strings[40] = "One of Hearts"
	card_strings[41] = "Two of hearts"
	card_strings[42] = "Three of Hearts"
	card_strings[43] = "Four of Hearts"
	card_strings[44] = "Five of Hearts"
	card_strings[45] = "Six of Hearts"
	card_strings[46] = "Seven of Hearts"
	card_strings[47] = "Eight of Hearts"
	card_strings[48] = "Nine of Hearts"
	card_strings[49] = "Ten of Hearts"
	card_strings[50] = "Jack of Hearts"
	card_strings[51] = "Queen of Hearts"
	card_strings[52] = "King of Hearts"

	card_strings[53] = "Joker1"
	card_strings[54] = "Joker2"

}

func (card Card) Get_Card() string {
	return card_strings[card.value]
}

func (card Card) Print_Card() {
	fmt.Println(card_strings[card.value])
}
