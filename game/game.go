package game

import (
	"bitbucket.org/rummy/cards"
	deck "bitbucket.org/rummy/deck"
	"bitbucket.org/rummy/utils"
	"fmt"
)

type Player struct {
	cards []cards.Card
}

type Game struct {
	thrown_cards *deck.Deck
	gdeck        *deck.Deck
	players      []*Player
	nplayers     int
	nos_of_cards int
	score        interface{}
}

func New_Game(n int, ncards int) *Game {
	var cut int
	g := &Game{}
	g.gdeck = deck.New_Deck(false)
	fmt.Println("Cut cards(Y/N)")
	text := utils.Read_Text()
	if utils.Is_Yes(text) {
		fmt.Println("Enter card no to cut at:")
		fmt.Scanf("%d", &cut)
		for cut < 1 || cut > 54 {
			fmt.Println("Enter card no to cut at:")
			fmt.Scanf("%d", &cut)
		}
		g.gdeck.Cut(cut)
	}
	g.nplayers = n
	for i := 0; i < g.nplayers; i++ {
		p := &Player{}
		g.players = append(g.players, p)
	}
	g.nos_of_cards = ncards
	for j := 0; j < g.nos_of_cards; j++ {
		for i := 0; i < g.nplayers; i++ {
			card := g.gdeck.Remove_Top()
			g.players[i].Insert_Card(card)

		}
	}

	for i := 0; i < g.nplayers; i++ {
		g.players[i].Show_Cards()
	}
	return g
}

func end_of_game(p *Player) bool {
	return false
}

func (p Player) Show_Cards() {
	var cont int

	fmt.Println("")
	for i := 0; i < len(p.cards); i++ {
		x := p.cards[i].Get_Card()
		fmt.Printf("%s ", x)
	}
	fmt.Scanf("%d", &cont)
}

func (p *Player) Insert_Card(c cards.Card) {
	p.cards = append(p.cards, c)
}

func (p *Player) Remove_Card() {
	var card_id int
	if p == nil {
		return
	}

	fmt.Println("Enter index of card to remove")
	fmt.Scanf("%d", &card_id)
	if len(p.cards) > card_id {
		p.cards = append(p.cards[:card_id], p.cards[card_id+1:]...)
	}
}

func (g *Game) Play_Game() {
	var i int
	var count int

	g.thrown_cards = deck.New_Deck(true)
	g.thrown_cards.Add_Top(g.gdeck.Remove_Top())

	for {
		count += 1
		if end_of_game(g.players[i]) {
			break
		}
		i = (i + 1) % g.nplayers
		fmt.Printf("Round %d, player #%d\n", count, i)
		g.players[i].Show_Cards()
		fmt.Println("")

		fmt.Println("Top of thrown card pile")
		visible_card := g.thrown_cards.Show_Deck_Top()
		visible_card.Print_Card()
		fmt.Println("Do you want to pick this card (Y/N)")
		if utils.Is_Yes(utils.Read_Text()) {
			g.thrown_cards.Remove_Top()
			g.players[i].Remove_Card()
			g.players[i].Insert_Card(visible_card)
		} else {
			new_card := g.gdeck.Remove_Top()
			new_card.Print_Card()
			fmt.Println("Do you want to pick this card (Y/N)")
			if utils.Is_Yes(utils.Read_Text()) {
				g.players[i].Remove_Card()
				g.players[i].Insert_Card(new_card)
			} else {
				g.thrown_cards.Add_Top(new_card)
			}
		}
		if g.gdeck.Is_Empty() {
			g.gdeck = g.thrown_cards
			g.gdeck.Set_Top(0)
			g.thrown_cards = nil
			g.thrown_cards = deck.New_Deck(true)
			g.thrown_cards.Add_Top(g.gdeck.Remove_Top())
		}
	}
}
